proc synth_Dlatch {PATH NAME WAY WIRE} {
    read_verilog src/$PATH/$NAME.v
    chparam -set "WAY" $WAY $NAME
    chparam -set "WIRE" $WIRE $NAME

    general $PATH $NAME
}

proc synth_Dlatch_all {WAY WIRE} {
    set PATH "memory/Dlatch"

    make_dir $PATH

    synth_Dlatch $PATH "Dlatch" $WAY $WIRE
    synth_Dlatch $PATH "Dlatch_rst" $WAY $WIRE
    synth_Dlatch $PATH "Dlatch_pre" $WAY $WIRE
    synth_Dlatch $PATH "Dlatch_rst_pre" $WAY $WIRE
}
